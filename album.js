cube(`Album`, {
    sql: `SELECT * FROM albums`,
    
    preAggregations: {
      // Pre-Aggregations definitions go here
      // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
    },
    
    // joins: {
    //   FilmActor: {
    //     relationship: `belongsTo`,
    //     sql: `${FilmActor}.actor_id = ${CUBE}.actor_id`
    //   }
    // },
    
    measures: {
      count: {
        type: `count`,
        drillMembers: [albumId, title]
      }
    },
    
    dimensions: {
  
      albumId: {
        sql: `AlbumId`,
        type: `number`,
        primaryKey: true,
      },
  
      title: {
        sql: `Title`,
        type: `string`
      },
      
    //   lastName: {
    //     sql: `last_name`,
    //     type: `string`
    //   },
      
    //   lastUpdate: {
    //     sql: `last_update`,
    //     type: `time`
    //   }
    },
    
    dataSource: `default`
  });